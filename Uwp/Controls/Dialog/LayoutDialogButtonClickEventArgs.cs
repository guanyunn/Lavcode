﻿namespace Hubery.Lavcode.Uwp.Controls.Dialog
{
    public class LayoutDialogButtonClickEventArgs
    {
        internal LayoutDialogButtonClickEventArgs() { }

        public bool Cancel { get; set; }
    }
}
