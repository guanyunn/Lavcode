﻿namespace Hubery.Lavcode.Uwp.Controls.Message
{
    public enum MessageType
    {
        Info,
        Primary,
        Warning,
        Danger
    }
}
