﻿using System;
using Windows.UI.Xaml.Data;

namespace Hubery.Lavcode.Uwp.Converters
{
    class NoBreakConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (string.IsNullOrEmpty(value?.ToString()))
            {
                return string.Empty;
            }

            if (!(value is string str))
            {
                str = value.ToString();
            }

            return str.Replace('\n', ' ').Replace('\r', ' ').Replace('\t', ' ');
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
