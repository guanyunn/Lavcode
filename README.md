﻿# Lavcode —— 免费开源的密码管理软件

**[主页](https://lavcode.hubery.wang) 查看更多详情**

本项目遵循 MIT 开源许可协议，您可以在自己的项目中自由使用，但需注明[来源](https://github.com/hbrwang)

## 获取代码

### Gitee

- git clone https://gitee.com/hbrwang/Lavcode.git
- 转至[Gitee](https://gitee.com/hbrwang/Lavcode)

### GitHub

- git clone https://github.com/hbrwang/Lavcode.git
- 转至[GitHub](https://github.com/hbrwang/Lavcode)

## 更多

更多信息，请查看 [https://lavcode.hubery.wang](https://lavcode.hubery.wang/pages/dev/start/)

## 截图

![截图1](https://lavcode.hubery.wang/screenshots/1.png)
![截图2](https://lavcode.hubery.wang/screenshots/2.png)
![截图3](https://lavcode.hubery.wang/screenshots/3.png)
![截图4](https://lavcode.hubery.wang/screenshots/4.png)
