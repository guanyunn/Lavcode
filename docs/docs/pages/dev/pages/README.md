---
title: 页面
---

主要页面都在 View 中，页面独有的子组件也都在这里。

## Auth

验证页面

## Feedback

反馈页面，列表使用了 7.1 的 `Comment`，点“反馈”会跳转的网页，在网页中使用了 GitTalk。也可在 本项目的 [Issues](https://github.com/hbrwang/Lavcode/issues/3) 看到。
这里的反馈列表，就是这条 Issue 中的 Comments

## FolderList

密码文件夹

## 未完待续
